package com.gitlab.davinkevin.jooq.thejoyofsql.podcast

import com.gitlab.davinkevin.podcastserver.database.tables.references.PODCAST
import org.jooq.*
import org.jooq.impl.DSL
import java.util.*

/**
 * Created by kevin on 01/11/2020
 */
class PodcastRepository(private val query: DSLContext) {

    private val youtubeField = PODCAST.METADATA.path("youtube", "channelId")

    fun findOne(id: UUID): YoutubePodcast? {
        return query
                .select(PODCAST.ID, PODCAST.TITLE, PODCAST.URL, youtubeField)
                .from(PODCAST)
                .where(PODCAST.ID.eq(id))
                .and(PODCAST.METADATA.hasKey("youtube"))
                .fetchOne { (id, title, url, channelId) -> YoutubePodcast(id!!, title!!, url!!, channelId!!) }
    }

    fun findAllYoutube(): List<YoutubePodcast> {
        return query
                .select(PODCAST.ID, PODCAST.TITLE, PODCAST.URL, youtubeField)
                .from(PODCAST)
                .where(PODCAST.METADATA.hasKey("youtube"))
                .fetch { (id, title, url, channelId) -> YoutubePodcast(id!!, title!!, url!!, channelId!!) }
    }
}

private fun Field<JSONB?>.path(vararg path: String): Field<String> {
    return DSL.field("{0}#>>{1}", String::class.java, this, DSL.array(*path))
}

private fun Field<JSONB?>.hasKey(vararg keys: String): Condition {
    return DSL.condition("{0} ??| {1}", this, DSL.array(*keys));
}

data class YoutubePodcast(val id: UUID, val title: String, val url: String, val channelId: String)
