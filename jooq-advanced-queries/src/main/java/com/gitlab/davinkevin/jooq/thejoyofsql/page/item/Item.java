package com.gitlab.davinkevin.jooq.thejoyofsql.page.item;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Created by kevin on 26/07/2020
 */
public record Item(UUID id,
                   String title,
                   String url,
                   OffsetDateTime pubDate,
                   OffsetDateTime downloadDate,
                   OffsetDateTime creationDate,
                   String description,
                   String mimeType,
                   Long length,
                   String fileName,
                   Status status,
                   Podcast podcast,
                   Cover cover
) {
    public record Podcast(UUID id, String title, String url) {}
    public record Cover(UUID id, String url, Integer width, Integer height) {}
}
