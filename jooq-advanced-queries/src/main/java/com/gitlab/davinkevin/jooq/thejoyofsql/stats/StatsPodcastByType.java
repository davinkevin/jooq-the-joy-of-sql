package com.gitlab.davinkevin.jooq.thejoyofsql.stats;

import java.util.Collection;

/**
 * Created by kevin on 18/07/2020
 */
public record StatsPodcastByType(
        String type,
        Collection<NumberOfItemByDate> values
) {}
