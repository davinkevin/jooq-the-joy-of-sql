package com.gitlab.davinkevin.jooq.thejoyofsql;

import com.gitlab.davinkevin.jooq.thejoyofsql.page.Page;
import com.gitlab.davinkevin.jooq.thejoyofsql.page.PageRequest;
import com.gitlab.davinkevin.jooq.thejoyofsql.page.PageSort;
import com.gitlab.davinkevin.jooq.thejoyofsql.page.item.Item;
import com.gitlab.davinkevin.jooq.thejoyofsql.page.item.Status;
import org.jooq.*;
import org.jooq.impl.DSL;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

import static com.gitlab.davinkevin.podcastserver.database.Tables.*;
import static org.jooq.impl.DSL.*;

/**
 * Created by kevin on 26/07/2020
 */
public record ItemRepository(DSLContext query) {

    public Page<Item> search(
            String q,
            List<String> tagNames,
            List<String> statuses,
            PageRequest page,
            UUID podcastId
    ) {

        var queryCondition = q.isEmpty()
                ? noCondition()
                : ITEM.TITLE.containsIgnoreCase(q).or(ITEM.DESCRIPTION.containsIgnoreCase(q));

        var tagIds = query
                .select(TAG.ID)
                .from(TAG)
                .where(TAG.NAME.in(tagNames))
                .fetchSet(Record1::value1);

        var tagsCondition = tagIds.isEmpty()
                ? noCondition()
                : tagIds
                        .stream()
                        .map(it -> value(it).in(query
                                .select(PODCAST_TAGS.TAGS_ID)
                                .from(PODCAST_TAGS)
                                .where(ITEM.PODCAST_ID.eq(PODCAST_TAGS.PODCASTS_ID)))
                        )
                        .reduce(noCondition(), DSL::and);
//              .collect(collectingAndThen(toList(), DSL::and));

        var statusesCondition = statuses.isEmpty()
                ? noCondition()
                : ITEM.STATUS.in(statuses);

        var podcastCondition = podcastId == null
                ? noCondition()
                : ITEM.PODCAST_ID.eq(podcastId);

        var conditions = and(queryCondition, tagsCondition, statusesCondition, podcastCondition);

        var fi = name("FILTERED_ITEMS").as(
                select(
                        ITEM.ID, ITEM.TITLE, ITEM.URL,
                        ITEM.PUB_DATE, ITEM.DOWNLOAD_DATE, ITEM.CREATION_DATE,
                        ITEM.DESCRIPTION, ITEM.MIME_TYPE, ITEM.LENGTH, ITEM.FILE_NAME, ITEM.STATUS,

                        ITEM.PODCAST_ID, ITEM.COVER_ID
                )
                        .from(ITEM)
                        .where(conditions)
                        .orderBy(toOrderBy(page.sort()), ITEM.ID.asc())
                        .limit(page.size() * page.page(), page.size().intValue())
        );

        var content = query
                .with(fi)
                .select(
                        fi.field(ITEM.ID), fi.field(ITEM.TITLE), fi.field(ITEM.URL),
                        fi.field(ITEM.PUB_DATE), fi.field(ITEM.DOWNLOAD_DATE), fi.field(ITEM.CREATION_DATE),
                        fi.field(ITEM.DESCRIPTION), fi.field(ITEM.MIME_TYPE), fi.field(ITEM.LENGTH),
                        fi.field(ITEM.FILE_NAME), fi.field(ITEM.STATUS),

                        PODCAST.ID, PODCAST.TITLE, PODCAST.URL,
                        COVER.ID, COVER.URL, COVER.WIDTH, COVER.HEIGHT
                )
                .from(
                        fi
                                .innerJoin(COVER).on(fi.field(ITEM.COVER_ID).eq(COVER.ID))
                                .innerJoin(PODCAST).on(fi.field(ITEM.PODCAST_ID).eq(PODCAST.ID))
                )
                .orderBy(toOrderBy(page.sort(), fi), fi.field(ITEM.ID))
                .fetch( r -> new Item(
                        r.get(fi.field(ITEM.ID)), r.get(fi.field(ITEM.TITLE)), r.get(fi.field(ITEM.URL)),
                        r.get(fi.field(ITEM.PUB_DATE)), r.get(fi.field(ITEM.DOWNLOAD_DATE)), r.get(fi.field(ITEM.CREATION_DATE)),
                        r.get(fi.field(ITEM.DESCRIPTION)), r.get(fi.field(ITEM.MIME_TYPE)), r.get(fi.field(ITEM.LENGTH)),
                        r.get(fi.field(ITEM.FILE_NAME)), Status.valueOf(r.get(fi.field(ITEM.STATUS))),

                        new Item.Podcast(r.get(PODCAST.ID), r.get(PODCAST.TITLE), r.get(PODCAST.URL)),
                        new Item.Cover(r.get(COVER.ID), r.get(COVER.URL), r.get(COVER.WIDTH), r.get(COVER.HEIGHT))
                ));

        var totalElements = query
                .select(countDistinct(ITEM.ID))
                .from(ITEM)
                .where(conditions)
                .fetchOne(countDistinct(ITEM.ID));

        return Page.of(
                content,
                totalElements,
                page
        );
    }

    private static SortField<OffsetDateTime> toOrderBy(PageSort sort) {
        return toOrderBy(sort, null);
    }
    private static SortField<OffsetDateTime> toOrderBy(PageSort sort, CommonTableExpression<Record13<UUID, String, String, OffsetDateTime, OffsetDateTime, OffsetDateTime, String, String, Long, String, String, UUID, UUID>> fi) {
        Function<Field<OffsetDateTime>, Field<OffsetDateTime>> toField = (v) -> fi == null ? v : fi.field(v);

        var field = sort.field().equals("downloadDate")
                ? toField.apply(ITEM.DOWNLOAD_DATE)
                : toField.apply(ITEM.PUB_DATE);

        return sort.direction().toUpperCase().equals("ASC")
                ? field.asc()
                : field.desc();
    }
}
