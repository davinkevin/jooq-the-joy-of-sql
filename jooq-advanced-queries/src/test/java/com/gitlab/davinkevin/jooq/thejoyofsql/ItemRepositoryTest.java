package com.gitlab.davinkevin.jooq.thejoyofsql;

import com.gitlab.davinkevin.jooq.thejoyofsql.page.PageRequest;
import com.gitlab.davinkevin.jooq.thejoyofsql.page.PageSort;
import com.gitlab.davinkevin.jooq.thejoyofsql.page.item.Item;
import org.jooq.SQLDialect;
import org.jooq.conf.RenderQuotedNames;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by kevin on 26/07/2020
 */
class ItemRepositoryTest {

    private static Connection connection;
    private ItemRepository repository;

    @BeforeAll
    public static void beforeAll() throws SQLException {
        connection = DriverManager.getConnection("jdbc:postgresql://postgres:5432/jooq", "jooq","jooq");
    }

    @BeforeEach
    public void beforeEach() {
        var query = DSL.using(connection, SQLDialect.POSTGRES
                , new Settings()
                        .withRenderFormatted(true)
                        .withRenderSchema(false)
                        .withRenderQuotedNames(RenderQuotedNames.NEVER)
        );
        repository = new ItemRepository(query);
    }

    @Test
    public void should_search_for_items() {
        /* GIVEN */
        var request = new PageRequest(0, 5, new PageSort("desc", "pubDate"));

        /* WHEN  */
        var result = repository.search(
                "",
                List.of(),
                List.of(),
                request,
                null
        );

        /* THEN  */
        assertThat(result.content())
                .hasSize(5)
                .extracting(Item::title)
                .containsExactly(
                    "Google Cloud Run avec Steren Giannini",
                    "Episode 4 – What’s Coming in PhpStorm 2020.2 – EAP Series",
                    "#44.exe vu par Jacques Ducloy – Le streaming aussi pour du texte – Valentin Baudot",
                    "Message à caractère informatique #6 - Déployer le service mesh du mandalorian sur Rancher",
                    "LCC 235 - Interview Micro Services avec @ygrenzinger et @khaledsouf"
                );
        assertThat(result.first()).isTrue();
        assertThat(result.last()).isFalse();
        assertThat(result.totalElements()).isEqualTo(2039);
        assertThat(result.totalPages()).isEqualTo(408);
    }

    @Test
    public void should_search_for_items_with_tag_and_status_and_no_podcast() {
        /* GIVEN */
        var request = new PageRequest(0, 5, new PageSort("desc", "pubDate"));

        /* WHEN  */
        var result = repository.search(
                "",
                List.of("Java"),
                List.of(),
                request,
                null
        );

        /* THEN  */
        assertThat(result.content())
                .hasSize(5)
                .extracting(Item::title)
                .containsExactly(
                        "Episode 4 – What’s Coming in PhpStorm 2020.2 – EAP Series",
                        "LCC 235 - Interview Micro Services avec @ygrenzinger et @khaledsouf",
                        "Profiling and Dynamic Program Analysis in Rider",
                        "Getting Started with Rider for #UnrealEngine",
                        "Customize the Look and Feel - Rider Essentials"
                );
        assertThat(result.first()).isTrue();
        assertThat(result.last()).isFalse();
        assertThat(result.totalElements()).isEqualTo(777);
        assertThat(result.totalPages()).isEqualTo(156);
    }

    @Test
    public void should_search_for_items_with_status_and_downloaded_with_no_podcast() {
        /* GIVEN */
        var request = new PageRequest(0, 5, new PageSort("desc", "pubDate"));

        /* WHEN  */
        var result = repository.search(
                "",
                List.of("Java"),
                List.of("FINISH"),
                request,
                null
        );

        /* THEN  */
        assertThat(result.content())
                .hasSize(4)
                .extracting(Item::title)
                .containsExactly(
                        "LCC 235 - Interview Micro Services avec @ygrenzinger et @khaledsouf",
                        "LCC 234 - EmmanuelBernard-As-A-Service, bientôt chez vous !",
                        "LCC 233 - Interview sur l'Event Storming avec Thomas Pierrain et Bruno Boucard",
                        "LCC 232 - Versions version Sloubi"
                );
        assertThat(result.first()).isTrue();
        assertThat(result.last()).isTrue();
        assertThat(result.totalElements()).isEqualTo(4);
        assertThat(result.totalPages()).isEqualTo(1);
    }

    @Test
    public void should_search_for_items_in_podcast() {
        /* GIVEN */
        var request = new PageRequest(0, 5, new PageSort("desc", "pubDate"));

        /* WHEN  */
        var result = repository.search(
                "",
                List.of(),
                List.of(),
                request,
                UUID.fromString("b08c7404-d9de-43d0-b1a3-25b8a26fa67d")
        );

        /* THEN  */
        assertThat(result.content())
                .hasSize(5)
                .extracting(Item::title)
                .containsExactly(
                        "LCC 235 - Interview Micro Services avec @ygrenzinger et @khaledsouf",
                        "LCC 234 - EmmanuelBernard-As-A-Service, bientôt chez vous !",
                        "LCC 233 - Interview sur l'Event Storming avec Thomas Pierrain et Bruno Boucard",
                        "LCC 232 - Versions version Sloubi",
                        "LCC 231 - Interview sur Vim avec Romain Lafourcade"
                );
        assertThat(result.first()).isTrue();
        assertThat(result.last()).isFalse();
        assertThat(result.totalElements()).isEqualTo(235);
        assertThat(result.totalPages()).isEqualTo(47);
    }
}
