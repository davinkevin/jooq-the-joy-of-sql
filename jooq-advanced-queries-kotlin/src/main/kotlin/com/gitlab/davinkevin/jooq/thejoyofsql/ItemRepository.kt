package com.gitlab.davinkevin.jooq.thejoyofsql

import com.gitlab.davinkevin.jooq.thejoyofsql.page.Page
import com.gitlab.davinkevin.jooq.thejoyofsql.page.PageRequest
import com.gitlab.davinkevin.jooq.thejoyofsql.page.Sort
import com.gitlab.davinkevin.jooq.thejoyofsql.page.item.Item
import com.gitlab.davinkevin.jooq.thejoyofsql.page.item.Status
import com.gitlab.davinkevin.podcastserver.database.tables.Cover.Companion.COVER
import com.gitlab.davinkevin.podcastserver.database.tables.Item.Companion.ITEM
import com.gitlab.davinkevin.podcastserver.database.tables.Podcast.Companion.PODCAST
import com.gitlab.davinkevin.podcastserver.database.tables.PodcastTags.Companion.PODCAST_TAGS
import com.gitlab.davinkevin.podcastserver.database.tables.Tag.Companion.TAG
import org.jooq.DSLContext
import org.jooq.Field
import org.jooq.impl.DSL
import org.jooq.impl.DSL.*
import java.net.URI
import java.util.*

/**
 * Created by kevin on 26/07/2020
 */
class ItemRepository(
        private val query: DSLContext
) {

    fun search(
            q: String = "",
            tagNames: List<String> = emptyList(),
            statuses: List<String> = emptyList(),
            page: PageRequest,
            podcastId: UUID? = null
    ): Page<Item> {

        val tagIds = query
                .select(TAG.ID)
                .from(TAG)
                .where(TAG.NAME.`in`(tagNames))
                .fetchSet { (v) -> v }

        val statusesCondition = if (statuses.isEmpty()) noCondition() else ITEM.STATUS.`in`(statuses)
        val tagsCondition = if (tagIds.isEmpty()) noCondition() else tagIds
                .map { value(it).`in`(query
                            .select(PODCAST_TAGS.TAGS_ID)
                            .from(PODCAST_TAGS)
                            .where(ITEM.PODCAST_ID.eq(PODCAST_TAGS.PODCASTS_ID)))
                }
                .reduce(DSL::and)

        val queryCondition = if (q.isBlank()) noCondition()
        else or(ITEM.TITLE.containsIgnoreCase(q), ITEM.DESCRIPTION.containsIgnoreCase(q))

        val podcastCondition = if (podcastId == null) noCondition()
        else ITEM.PODCAST_ID.eq(podcastId)

        val conditions = and(statusesCondition, tagsCondition, queryCondition, podcastCondition)

        val fi = name("FILTERED_ITEMS").`as`(
                select(
                        ITEM.ID, ITEM.TITLE, ITEM.URL,
                        ITEM.PUB_DATE, ITEM.DOWNLOAD_DATE, ITEM.CREATION_DATE,
                        ITEM.DESCRIPTION, ITEM.MIME_TYPE, ITEM.LENGTH, ITEM.FILE_NAME, ITEM.STATUS,
                        ITEM.PODCAST_ID, ITEM.COVER_ID
                )
                        .from(ITEM)
                        .where(conditions)
                        .orderBy(page.sort.toOrderBy(ITEM.DOWNLOAD_DATE, ITEM.PUB_DATE), ITEM.ID.asc())
                        .limit(page.size * page.page, page.size)
        )

        val content: List<Item> = query
                .with(fi)
                .select(
                        fi.field(ITEM.ID), fi.field(ITEM.TITLE), fi.field(ITEM.URL),
                        fi.field(ITEM.PUB_DATE), fi.field(ITEM.DOWNLOAD_DATE), fi.field(ITEM.CREATION_DATE),
                        fi.field(ITEM.DESCRIPTION), fi.field(ITEM.MIME_TYPE), fi.field(ITEM.LENGTH),
                        fi.field(ITEM.FILE_NAME), fi.field(ITEM.STATUS),
                        PODCAST.ID, PODCAST.TITLE, PODCAST.URL,
                        COVER.ID, COVER.URL, COVER.WIDTH, COVER.HEIGHT
                )
                .from(
                        fi
                                .innerJoin(COVER).on(fi.field(ITEM.COVER_ID)?.eq(COVER.ID))
                                .innerJoin(PODCAST).on(fi.field(ITEM.PODCAST_ID)?.eq(PODCAST.ID))
                )
                .orderBy(page.sort.toOrderBy(fi.field(ITEM.DOWNLOAD_DATE), fi.field(ITEM.PUB_DATE)))
                .fetch { (
                                id, title, url,
                                pubDate, downloadDate, creationDate,
                                description, mimeType, length,
                                fileName, status,

                                podcastId, podcastTitle, podcastUrl,
                                coverId, coverUrl, coverWidth, coverHeight
                        ) -> Item(
                                id!!, title!!, url,
                                pubDate, downloadDate, creationDate,
                                description, mimeType!!, length, fileName, Status.valueOf(status!!),

                                Item.Podcast(podcastId!!, podcastTitle!!, podcastUrl),
                                Item.Cover(coverId!!, URI(coverUrl!!), coverWidth!!, coverHeight!!)
                ) }

        val totalElements: Int = query
                .select<Int>(countDistinct(ITEM.ID))
                .from(ITEM)
                .where(conditions)
                .fetchOne(countDistinct(ITEM.ID))!!

        return Page.of(
                content = content,
                totalElements = totalElements,
                page = page
        )
    }

    private fun <T> Sort.toOrderBy(downloadDate: Field<T>?, defaultField: Field<T>?) = when(field) {
        "downloadDate" -> downloadDate
        else -> defaultField
    }.let { when(direction.toUpperCase()) {
        "ASC" -> it?.asc()
        else -> it?.desc()
    } }

}
