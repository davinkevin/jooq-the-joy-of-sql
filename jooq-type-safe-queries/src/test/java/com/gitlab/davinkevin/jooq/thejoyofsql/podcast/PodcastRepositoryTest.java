package com.gitlab.davinkevin.jooq.thejoyofsql.podcast;

import org.jooq.SQLDialect;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by kevin on 11/07/2020
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PodcastRepositoryTest {

    private static Connection connection;
    private PodcastRepository repository;

    @BeforeAll
    public static void beforeAll() throws SQLException {
        connection = DriverManager.getConnection("jdbc:postgresql://postgres:5432/jooq", "jooq","jooq");
    }

    @BeforeEach
    public void beforeEach() {
        var query = DSL.using(connection, SQLDialect.POSTGRES, new Settings()
                .withRenderFormatted(true)
                .withRenderSchema(false)
        );
        repository = new PodcastRepository(query);
    }

    @Test
    @Order(100)
    public void should_find_all_podcasts() {
        /* GIVEN */
        /* WHEN  */
        var podcasts = repository.findAll();
        /* THEN  */
        assertThat(podcasts).containsExactly(
            new Podcast(UUID.fromString("05863ea0-1bd7-4d4a-9c72-283fdfe4a393"), "Ladybug Podcast", "https://pinecast.com/feed/ladybug-podcast"),
            new Podcast(UUID.fromString("20cb466b-431e-4571-9001-8edc1f2c8dfa"), "Kubernetes Podcast from Google", "https://kubernetespodcast.com/feeds/audio.xml"),
            new Podcast(UUID.fromString("2e8a6a65-d1ab-4128-b355-edd5e11bddee"), "JUG Leaders", "https://www.youtube.com/channel/UCmq9YQtlwsew4rAeO3H-ZLg/videos"),
            new Podcast(UUID.fromString("44d82b60-41b0-4e21-a252-c680ef366768"), "Message à caractère informatique", "https://www.youtube.com/watch?v=ug1z_D9jh-Q&list=PLvjEkX1131rBpr7T8t6Zwdhe5RlpsT3Ci"),
            new Podcast(UUID.fromString("4c977566-8226-4093-b0ad-8332eb297f47"), "Talking Kotlin", "http://feeds.soundcloud.com/users/soundcloud:users:280353173/sounds.rss"),
            new Podcast(UUID.fromString("89241656-5be9-4a09-821f-cec8ed4a7d6a"), "NoLimitSecu", "https://www.nolimitsecu.fr/feed/podcast/"),
            new Podcast(UUID.fromString("96365a7c-8875-44f1-9f25-9b6fadbb4ec2"), "Goood Morning Web", "http://feeds.soundcloud.com/users/soundcloud:users:504680106/sounds.rss"),
            new Podcast(UUID.fromString("b08c7404-d9de-43d0-b1a3-25b8a26fa67d"), "Les Cast Codeurs", "http://lescastcodeurs.libsyn.com/rss"),
            new Podcast(UUID.fromString("cc16e456-8140-48f2-acfd-aed4eb2558f9"), "Electro Monkeys", "https://feeds.buzzsprout.com/926791.rss"),
            new Podcast(UUID.fromString("cf615560-5f61-42f2-a931-0bf43c6823cd"), "JetBrainsTV", "http://www.youtube.com/user/JetBrainsTV"),
            new Podcast(UUID.fromString("dbcefbca-113a-4493-86aa-060bf058ba20"), "GDG France", "https://www.youtube.com/user/francegdg/videos"),
            new Podcast(UUID.fromString("e4335987-9d2b-4e24-95fb-93164c9d3809"), "IFTTD - If This Then Dev", "https://rss.art19.com/ifttd-if-this-then-dev")
        );
    }

    @Test
    @Order(100)
    public void should_find_all_podcasts_with_cover() {
        /* GIVEN */
        var ladyBug = new PodcastWithCover(
                UUID.fromString("05863ea0-1bd7-4d4a-9c72-283fdfe4a393"),
                "Ladybug Podcast",
                "https://pinecast.com/feed/ladybug-podcast",
                new PodcastWithCover.Cover(
                        "https://storage.pinecast.net/podcasts/covers/c617f912-02e9-488c-898e-59a87bdccd2f/ladybug_logo.png",
                        3000,
                        3000
                )
        );
        var kubernetesPodcast = new PodcastWithCover(
                UUID.fromString("20cb466b-431e-4571-9001-8edc1f2c8dfa"),
                "Kubernetes Podcast from Google",
                "https://kubernetespodcast.com/feeds/audio.xml",
                new PodcastWithCover.Cover(
                        "https://kubernetespodcast.com/_assets/images/Kubernetes-Podcast-Logo_1400x1400.png",
                        1400,
                        1400
                )
        );
        var jugLeaders = new PodcastWithCover(
                UUID.fromString("2e8a6a65-d1ab-4128-b355-edd5e11bddee"),
                "JUG Leaders",
                "https://www.youtube.com/channel/UCmq9YQtlwsew4rAeO3H-ZLg/videos",
                new PodcastWithCover.Cover(
                        "https://yt3.ggpht.com/a/AATXAJxN9ccLfZS1dlzEZrNC7Nqk3e_QZ1TAeyJaYncV=s900-c-k-c0xffffffff-no-rj-mo",
                        900,
                        900
                )
        );
        /* WHEN  */
        var podcasts = repository.findThreeWithCover();
        /* THEN  */
        assertThat(podcasts)
                .containsExactly(ladyBug, kubernetesPodcast, jugLeaders);
    }

    @Test
    @Order(100)
    public void should_find_all_podcasts_with_cover_simplified() {
        /* GIVEN */
        var ladyBug = new PodcastWithCover(
                UUID.fromString("05863ea0-1bd7-4d4a-9c72-283fdfe4a393"),
                "Ladybug Podcast",
                "https://pinecast.com/feed/ladybug-podcast",
                new PodcastWithCover.Cover(
                        "https://storage.pinecast.net/podcasts/covers/c617f912-02e9-488c-898e-59a87bdccd2f/ladybug_logo.png",
                        3000,
                        3000
                )
        );
        var kubernetesPodcast = new PodcastWithCover(
                UUID.fromString("20cb466b-431e-4571-9001-8edc1f2c8dfa"),
                "Kubernetes Podcast from Google",
                "https://kubernetespodcast.com/feeds/audio.xml",
                new PodcastWithCover.Cover(
                        "https://kubernetespodcast.com/_assets/images/Kubernetes-Podcast-Logo_1400x1400.png",
                        1400,
                        1400
                )
        );
        var jugLeaders = new PodcastWithCover(
                UUID.fromString("2e8a6a65-d1ab-4128-b355-edd5e11bddee"),
                "JUG Leaders",
                "https://www.youtube.com/channel/UCmq9YQtlwsew4rAeO3H-ZLg/videos",
                new PodcastWithCover.Cover(
                        "https://yt3.ggpht.com/a/AATXAJxN9ccLfZS1dlzEZrNC7Nqk3e_QZ1TAeyJaYncV=s900-c-k-c0xffffffff-no-rj-mo",
                        900,
                        900
                )
        );
        /* WHEN  */
        var podcasts = repository.findAllWithCoverSimplified();
        /* THEN  */
        assertThat(podcasts)
                .containsExactly(ladyBug, kubernetesPodcast, jugLeaders);
    }

    @Test
    @Order(100)
    public void should_find_one_by_uuid() {
        /* GIVEN */
        var castCodeursUUID = UUID.fromString("b08c7404-d9de-43d0-b1a3-25b8a26fa67d");
        /* WHEN  */
        Optional<Podcast> castCodeurs = repository.findOne(castCodeursUUID);
        /* THEN  */
        assertThat(castCodeurs).isPresent()
                .contains(new Podcast(
                        UUID.fromString("b08c7404-d9de-43d0-b1a3-25b8a26fa67d"),
                        "Les Cast Codeurs",
                        "http://lescastcodeurs.libsyn.com/rss")
                );
    }

    @Test
    @Order(1000)
    public void should_insert() {
        /* GIVEN */
        /* WHEN  */
        var podcast = repository
                .create(new Podcast(UUID.randomUUID(), "KubeCon and CloudNativeCon Europe 2020", "https://www.youtube.com/playlist?list=PLj6h78yzYM2O1wlsM-Ma-RYhfT5LKq0XC"));
        /* THEN  */
        assertThat(podcast.id()).isNotNull();
        assertThat(podcast.title()).isEqualTo("KubeCon and CloudNativeCon Europe 2020");
        assertThat(podcast.url()).isEqualTo("https://www.youtube.com/playlist?list=PLj6h78yzYM2O1wlsM-Ma-RYhfT5LKq0XC");
    }
}
