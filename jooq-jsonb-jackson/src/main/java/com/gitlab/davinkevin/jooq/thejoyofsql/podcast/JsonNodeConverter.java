package com.gitlab.davinkevin.jooq.thejoyofsql.podcast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jooq.Converter;
import org.jooq.JSONB;

/**
 * Created by kevin on 18/07/2020
 */
public class JsonNodeConverter implements Converter<JSONB, JsonNode> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public JsonNode from(JSONB t) {
        if (t == null) {
            return null;
        }

        try {
            return mapper.readTree(t.data());
        } catch (JsonProcessingException e) {
            throw new RuntimeException("jackson parsing failed…", e);
        }
    }

    @Override
    public JSONB to(JsonNode u) {
        return u == null ? null : JSONB.valueOf(u.toString());
    }

    @Override
    public Class<JSONB> fromType() {
        return JSONB.class;
    }

    @Override
    public Class<JsonNode> toType() {
        return JsonNode.class;
    }
}
