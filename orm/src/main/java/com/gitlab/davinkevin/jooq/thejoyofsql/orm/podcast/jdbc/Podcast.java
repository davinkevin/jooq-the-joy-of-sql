package com.gitlab.davinkevin.jooq.thejoyofsql.orm.podcast.jdbc;

import java.util.UUID;

/**
 * Created by kevin on 22/08/2020
 */
public record Podcast(UUID id, String title, String url) {}
