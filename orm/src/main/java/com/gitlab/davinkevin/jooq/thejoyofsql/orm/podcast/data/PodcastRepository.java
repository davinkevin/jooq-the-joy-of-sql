package com.gitlab.davinkevin.jooq.thejoyofsql.orm.podcast.data;

import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Created by kevin on 07/11/2020
 */
public interface PodcastRepository extends Repository<Podcast, UUID> {

    List<Podcast> findByTitleAndUrl(String title, String url);

}
