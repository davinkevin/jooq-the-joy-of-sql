package com.gitlab.davinkevin.jooq.thejoyofsql;

import org.jooq.DSLContext;

import java.util.Optional;
import java.util.UUID;
import java.util.List;

import static com.gitlab.davinkevin.podcastserver.database.Tables.ITEM;
import static com.gitlab.davinkevin.podcastserver.database.Tables.PODCAST;
import static org.jooq.Records.mapping;
import static org.jooq.impl.DSL.multiset;
import static org.jooq.impl.DSL.select;

/**
 * Created by kevin on 18/07/2020
 */
public class PodcastRepository {

    private final DSLContext query;

    public PodcastRepository(DSLContext query) {
        this.query = query;
    }

    public Optional<Podcast> findWithItems(UUID id) {
        return query
                .select(
                        PODCAST.ID, PODCAST.TITLE, PODCAST.URL,
                        multiset(
                                select(ITEM.ID, ITEM.TITLE, ITEM.URL)
                                        .from(ITEM)
                                        .where(ITEM.PODCAST_ID.eq(PODCAST.ID))
                        )
                                .as("items")
                                .convertFrom(it -> it.map(mapping(Podcast.Item::new)))
                )
                .from(PODCAST)
                .where(PODCAST.ID.eq(id))
                .fetchOptional(mapping(Podcast::new));
    }

    public List<Podcast> findAllWithItems() {
        return query
                .select(
                        PODCAST.ID, PODCAST.TITLE, PODCAST.URL,
                        multiset(
                                select(ITEM.ID, ITEM.TITLE, ITEM.URL)
                                        .from(ITEM)
                                        .where(ITEM.PODCAST_ID.eq(PODCAST.ID))
                        )
                                .as("items")
                                .convertFrom(it -> it.map(mapping(Podcast.Item::new)))
                )
                .from(PODCAST)
                .fetch(mapping(Podcast::new));
    }
}
