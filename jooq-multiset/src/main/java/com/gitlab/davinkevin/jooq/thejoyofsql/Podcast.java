package com.gitlab.davinkevin.jooq.thejoyofsql;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by kevin on 16/04/2022
 */
public record Podcast(UUID id, String title, String url, Collection<Item> items) {
    public record Item(UUID id, String title, String url) {}
}
