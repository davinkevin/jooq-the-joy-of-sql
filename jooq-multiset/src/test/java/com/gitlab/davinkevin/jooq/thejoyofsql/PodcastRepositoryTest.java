package com.gitlab.davinkevin.jooq.thejoyofsql;

import org.jooq.SQLDialect;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by kevin on 16/04/2022
 */
class PodcastRepositoryTest {

    private static Connection connection;
    private PodcastRepository repository;

    @BeforeAll
    public static void beforeAll() throws SQLException {
        connection = DriverManager.getConnection("jdbc:postgresql://postgres:5432/jooq", "jooq","jooq");
    }

    @BeforeEach
    public void beforeEach() {
        var query = DSL.using(connection, SQLDialect.POSTGRES, new Settings().withRenderFormatted(true));
        repository = new PodcastRepository(query);
    }

    @Test
    public void should_find_one_podcast_with_items() {
        /* GIVEN */
        var id = UUID.fromString("b08c7404-d9de-43d0-b1a3-25b8a26fa67d");
        /* WHEN  */
        var podcast = repository.findWithItems(id).orElseThrow();
        /* THEN  */
        assertThat(podcast.id()).isEqualTo(id);
        assertThat(podcast.title()).isEqualTo("Les Cast Codeurs");
        assertThat(podcast.url()).isEqualTo("http://lescastcodeurs.libsyn.com/rss");
        assertThat(podcast.items()).hasSize(235);
    }

    @Test
    public void should_find_all_podcasts_with_items() {
        /* GIVEN */
        /* WHEN  */
        var podcasts = repository.findAllWithItems();
        /* THEN  */
        assertThat(podcasts).hasSize(12);
    }

}
